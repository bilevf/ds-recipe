package com.bilev.common.config;

/**
 * Application constants class
 */
public class AppConstants {

    /**
     * Module log ID.
     */
    public static final String MODULE_ID = "ds_recipe";

    /**
     * Data model schema name.
     */
    public static final String DATA_MODEL_SCHEMA_NAME = "ds_recipe";
}

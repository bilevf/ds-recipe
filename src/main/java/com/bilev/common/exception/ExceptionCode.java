package com.bilev.common.exception;

/**
 * Application exception codes
 */
public enum ExceptionCode {

    /**
     * Requested object not found
     */
    NOT_FOUND,

    /**
     * Illegal object state
     */
    ILLEGAL_STATE
}

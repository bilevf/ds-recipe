package com.bilev.common.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * Application exception dto class
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionDto {

    public ExceptionCode exceptionCode;

    public String message;
}

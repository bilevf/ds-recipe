package com.bilev.common.log;

import com.bilev.common.config.AppConstants;
import org.jboss.logging.Logger;

/**
 * Application service for customizing log messages.
 */
public class AppLogger {

    private static final String LOG_MESSAGE = "[SRV:%s, TraceId:%s] %s %s";
    private final Logger logger;

    /**
     * Creates the application logger based on the logger implementation.
     *
     * @param logger the logger implementation
     */
    public AppLogger(final Logger logger) {
        this.logger = logger;
    }

    /**
     * Logs on debug level.
     *
     * @param logId the log id {@link AppLogId}
     * @param msg   log message
     */
    public void debug(
            final AppLogId logId,
            final String msg) {
        logger.debug(build(logId, msg));
    }

    /**
     * Logs on info level.
     *
     * @param logId the log id {@link AppLogId}
     * @param msg   log message
     */
    public void info(
            final AppLogId logId,
            final String msg) {
        logger.info(build(logId, msg));
    }

    /**
     * Logs on warning level.
     *
     * @param logId the log id {@link AppLogId}
     * @param msg   log message
     */
    public void warn(
            final AppLogId logId,
            final String msg) {
        logger.warn(build(logId, msg));
    }

    /**
     * Logs on warning level with error.
     *
     * @param logId the log id {@link AppLogId}
     * @param msg   log message
     */
    public void warn(
            final AppLogId logId,
            final String msg,
            final Throwable th) {
        logger.warn(build(logId, msg), th);
    }

    /**
     * Logs on error level.
     *
     * @param logId the log id {@link AppLogId}
     * @param msg   log message
     */
    public void error(
            final AppLogId logId,
            final String msg) {
        logger.error(build(logId, msg));
    }

    /**
     * Logs on error level with error.
     *
     * @param logId the log id {@link AppLogId}
     * @param msg   log message
     */
    public void error(
            final AppLogId logId,
            final String msg,
            final Throwable th) {
        logger.error(build(logId, msg), th);
    }

    /**
     * Build log message with trace and error ids.
     *
     * @param logId the log id {@link AppLogId}
     * @param msg   log message
     * @return log message with ids
     */
    private String build(
            final AppLogId logId,
            final String msg) {
        return sanitize(getMessage(logId.toString(), msg));

    }

    /**
     * Sanitize message for logging after FORTIFY by replacing new lines
     *
     * @param msg source message
     * @return sanitized message
     */
    private String sanitize(final String msg) {
        return msg.replace('\n', '_').replace('\r', '_');
    }

    /**
     * Get log message with specific data
     *
     * @param logId the log id {@link AppLogId}
     * @param msg   log message
     * @return formatted message
     */
    private String getMessage(
            final String logId,
            final String msg) {
        return String.format(LOG_MESSAGE, AppConstants.MODULE_ID, "traceId", logId, msg);
    }

}

package com.bilev.common.log;

import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 * Producer for the logger instances to allow injection of the logger over CDI.
 */
@Dependent
public class LoggerProducer {

    /**
     * Produce an instance of the logger for the class.
     *
     * @param injectionPoint injection point to fetch the class name from
     * @return application logger instance
     */
    @Produces
    public AppLogger produceLogger(final InjectionPoint injectionPoint) {
        final String clazzName = injectionPoint.getMember().getDeclaringClass().getName();
        final Logger logger = Logger.getLogger(clazzName);
        return new AppLogger(logger);
    }

}

package com.bilev.common.log;

/**
 * Application log codes.
 */
public enum AppLogId {

    /**
     * Application request log code.
     */
    REQUEST_LOG,

    /**
     * Application response log code.
     */
    RESPONSE_LOG,

    /**
     * Application info log code.
     */
    INFO_LOG,
    ;
}

package com.bilev.common.log;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Application logging interceptor class.
 */
@Logged
@Priority(1000)
@Interceptor
public class LoggingInterceptor {

    @Inject
    AppLogger log;

    final Jsonb jsonb = JsonbBuilder.create();

    /**
     * {@inheritDoc}
     */
    @AroundInvoke
    Object intercept(final InvocationContext ic) throws Exception {
        final long start = System.nanoTime();
        final String methodName = getLoggedMethodName(ic);

        try {
            log.info(AppLogId.REQUEST_LOG, methodName + " called with " + getParametersString(ic) + " attributes");
            final Object result = ic.proceed();
            log.info(AppLogId.RESPONSE_LOG, methodName + " answered with " + jsonb.toJson(result) + " result");

            return result;
        } catch (final Throwable e) {
            log.info(AppLogId.RESPONSE_LOG, methodName + " failed with " + e + " exception");
            throw e;
        } finally {
            final long durationInMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start);
            log.info(AppLogId.RESPONSE_LOG,methodName + " executed in " + durationInMs + " ms");
        }
    }

    /**
     * Creates printable name for intercepted method
     *
     * @param   ic invocation context
     * @return  readable method name
     */
    private String getLoggedMethodName(final InvocationContext ic) {
        final String methodName = ic.getMethod().getName();
        final String className = ic.getMethod().getDeclaringClass().getName();

        return className + ":" + methodName;
    }

    /**
     * Creates input parameters string
     *
     * @param ic invocation context
     * @return printable string
     */
    private String getParametersString(final InvocationContext ic) {
        final Object[] parameters = ic.getParameters();
        if (parameters == null || parameters.length == 0) {
            return "";
        }
        return "[" + Arrays.stream(parameters).map(jsonb::toJson).collect(Collectors.joining(", ")) + "]";
    }
}

package com.bilev.recipe.dto;

import com.bilev.recipe.entity.basic.LanguageCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Tag Dto class.
 */
@Schema(description="Dto for tag")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TagDto {

    @Schema(description = "Tag identifier (null for creation)", nullable = true,
            example = "f79c0012-bf63-43b0-9d11-da0549b13a7e")
    public UUID id;

    @Schema(description = "Entity modified at time", readOnly = true, example = "")
    public Date modifiedAt;

    @Schema(description = "Who modified entity", readOnly = true, example = "")
    public Date modifiedBy;

    @Schema(description = "Map of localized name")
    @NotNull
    @Builder.Default
    public Map<LanguageCode, String> translations = new HashMap<>();
}

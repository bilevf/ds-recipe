package com.bilev.recipe.service;

import com.bilev.common.exception.ApplicationException;
import com.bilev.common.exception.ExceptionCode;
import com.bilev.recipe.dto.TagDto;
import com.bilev.recipe.entity.TagEntity;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Tag service class.
 */
@Dependent
public class TagService {

    @Inject
    ConverterService converter;

    /**
     * Gets all tags since date.
     *
     * @param date since date
     * @return collection of {@link TagDto}
     */
    public Collection<TagDto> getTags(final Date date) {
        return TagEntity.find("updatedAt > ?1", date).stream()
                .map(entity -> converter.toDto((TagEntity) entity))
                .collect(Collectors.toList());
    }

    /**
     * Gets tag by id.
     *
     * @param id tag identifier
     * @return {@link TagDto}
     */
    public TagDto getTag(final UUID id) {
        return converter.toDto(TagEntity.get(id));
    }

    /**
     * Creates or updates tag.
     *
     * @param dto {@link TagDto}
     * @return {@link TagDto}
     */
    @Transactional
    public TagDto updateTag(final TagDto dto) {
        final TagEntity entity = converter.toEntity(dto);
        if (entity.id == null) {
            entity.persist();
        }
        return converter.toDto(entity);
    }

    /**
     * Deletes tag by id.
     *
     * @param id tag identifier
     */
    @Transactional
    public void removeTag(final UUID id) {
        final TagEntity entity = TagEntity.get(id);
        if (!entity.ingredients.isEmpty() || !entity.recipes.isEmpty()) {
            throw new ApplicationException(ExceptionCode.ILLEGAL_STATE, "Tag has dependencies");
        }
        entity.delete();
    }
}

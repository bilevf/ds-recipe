package com.bilev.recipe.service;

import com.bilev.common.exception.ApplicationException;
import com.bilev.common.exception.ExceptionCode;
import com.bilev.recipe.dto.IngredientDto;
import com.bilev.recipe.entity.IngredientEntity;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Ingredient service class.
 */
@Dependent
public class IngredientService {

    @Inject
    ConverterService converter;

    /**
     * Gets all ingredients since date.
     *
     * @param date since date
     * @return collection of {@link IngredientDto}
     */
    public Collection<IngredientDto> getIngredients(final Date date) {
        return IngredientEntity.find("updatedAt > ?1", date).stream()
                .map(entity -> converter.toDto((IngredientEntity) entity))
                .collect(Collectors.toList());
    }

    /**
     * Gets ingredient by id.
     *
     * @param id ingredient identifier
     * @return {@link IngredientDto}
     */
    public IngredientDto getIngredient(final UUID id) {
        return converter.toDto(IngredientEntity.get(id));
    }

    /**
     * Creates or updates ingredient.
     *
     * @param dto {@link IngredientDto}
     * @return {@link IngredientDto}
     */
    @Transactional
    public IngredientDto updateIngredient(final IngredientDto dto) {
        final IngredientEntity entity = converter.toEntity(dto);
        if (entity.id == null) {
            entity.persist();
        }
        return converter.toDto(entity);
    }

    /**
     * Deletes ingredient by id.
     *
     * @param id ingredient identifier
     */
    @Transactional
    public void removeIngredient(final UUID id) {
        final IngredientEntity entity = IngredientEntity.get(id);
        if (!entity.recipeIngredients.isEmpty()) {
            throw new ApplicationException(ExceptionCode.ILLEGAL_STATE, "Ingredient has dependencies");
        }
        entity.delete();
    }
}

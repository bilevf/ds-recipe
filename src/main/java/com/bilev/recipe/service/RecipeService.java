package com.bilev.recipe.service;

import com.bilev.recipe.dto.RecipeDto;
import com.bilev.recipe.entity.RecipeEntity;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.transaction.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Recipe service class.
 */
@Dependent
public class RecipeService {

    @Inject
    ConverterService converter;

    /**
     * Gets recipe by id.
     *
     * @param id recipe identifier
     * @return {@link RecipeDto}
     */
    public RecipeDto getRecipe(final UUID id) {
        return converter.toDto(RecipeEntity.get(id));
    }

    /**
     * Gets all recipes since date.
     *
     * @param date since date
     * @return collection of {@link RecipeDto}
     */
    public Collection<RecipeDto> getRecipes(final Date date) {
        return RecipeEntity.find("updatedAt > ?1", date).stream()
                .map(entity -> converter.toDto((RecipeEntity) entity))
                .collect(Collectors.toList());
    }

    /**
     * Creates or updates recipe.
     *
     * @param dto {@link RecipeDto}
     * @return {@link RecipeDto}
     */
    @Transactional
    public RecipeDto updateRecipe(final RecipeDto dto) {
        final RecipeEntity entity = converter.toEntity(dto);
        if (entity.id == null) {
            entity.persist();
        }
        return converter.toDto(entity);
    }
}

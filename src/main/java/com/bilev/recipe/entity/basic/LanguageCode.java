package com.bilev.recipe.entity.basic;

/**
 * Application supported languages
 */
public enum LanguageCode {
    EN,
    FR,
    DE,
    ES,
    JA,
    RU,
    IT
}

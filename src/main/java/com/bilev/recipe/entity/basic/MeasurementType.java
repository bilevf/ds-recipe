package com.bilev.recipe.entity.basic;

/**
 * Application supported measurement types
 */
public enum MeasurementType {
    PIECE,
    GRAM,
    MILLILITER;
}
